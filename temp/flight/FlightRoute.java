package flight;

public class FlightRoute {

    private String name;
    private String cityDeparture;
    private String cityArrival;
//    private String typeAircraft;

    private FlightRoute(){

    }
    /** Конструктор только с параметрами*/
    public FlightRoute(String name,String cityArrival, String cityDeparture){
        this.name = name;
        this.cityArrival = cityArrival;
        this.cityDeparture = cityDeparture;
//        this.typeAircraft = typeAircraft;
    }

    /** Установить имя маршрута*/
    public void setName(String name){
        this.name = name;
    }
    /** Установить город прибытия*/
    public void setCityArrival(String cityArrival){
        this.cityArrival = cityArrival;
    }
    /** Установить город отправления*/
    public void setCityDeparture(String cityDeparture){
        this.cityDeparture = cityDeparture;
    }
    /** Получить имя маршрута*/
    public String getName(){
        return this.name;
    }
    /** Получить город отбытия*/
    public String getCityDeparture(){
        return this.cityDeparture;
    }
    /** Получить город прибытия*/
    public String getCityArrival(){
        return this.cityArrival;
    }



}
