package flight;
import java.time.LocalDateTime;


public class Flight {
    /** Владелец рейса - название компании*/
    private String owner;
    /** Рейс*/
    private String nameFlight;
    /** Город отправитель*/
    private String cityDeparture;
    /** Город пребывания*/
    private String cityArrival;
    /** Время отправления*/
    private LocalDateTime dateTimeDeparture;
    /** Время прибытия*/
    private LocalDateTime dateTimeArrival;
    /** Дистанция между городами*/
//    private int distance;
    /** Статус рейса(ожидается, задержен, отменен)*/
    private String status;


    /** Конструктор по умолчанию не доступен*/
    private Flight(){
    }
    /** Конструктор только с параметрами*/
    public Flight(String nameFlight, String owner,String cityDeparture, String cityArrival,
                  LocalDateTime dateTimeDeparture, int distance) {
        this.nameFlight = nameFlight;
        this.cityDeparture = cityDeparture;
        this.cityArrival = cityArrival;
        this.dateTimeDeparture = dateTimeDeparture;
//        this.distance = distance;
        this.owner = owner;
    }

//    /** Получить владельца рейса, то есть имя авиакомпании*/
//    public String getOwner() {
//        return this.owner;
//    }
//    /** Получить номер-название рейса*/
//    public String getNameFlight() {
//        return this.nameFlight;
//    }
//    /** Получить имя города-отправления*/
//    public String getCityDeparture() {
//        return this.cityDeparture;
//    }
//    /** Получить имя города-прибытия*/
//    public String getCityArrival() {
//        return cityArrival;
//    }
//    /** Получить дату и время отправления*/
//    public LocalDateTime getDateTimeDeparture() {
//        return dateTimeDeparture;
//    }
//    /** Получить дистанцию между городами*/
//    public int getDistance() {
//        return distance;
//    }
//    /** Установить номер-название рейса*/
//    public void setNameFlight(String nameFlight) {
//        this.nameFlight = nameFlight;
//    }
//    /** Установить город отправления*/
//    public void setCityDeparture(String cityDeparture) {
//        this.cityDeparture = cityDeparture;
//    }
//    /** Установить дистанцию между городами*/
//    public void setDistance(int distance) {
//        this.distance = distance;
//    }
//    /** Установить город прибытия*/
//    public void setCityArrival(String cityArrival) {
//        this.cityArrival = cityArrival;
//    }
//    /** Установить время и дату отправления*/
//    public void setDateTimeDeparture(LocalDateTime dateTimeDeparture) {
//        this.dateTimeDeparture = dateTimeDeparture;
//    }
}
