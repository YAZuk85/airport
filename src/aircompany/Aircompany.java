package aircompany;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import aircraft.AircraftPassenger;
import flight.FlightRoute;

public class Aircompany {
    /** Имя компании*/
    private String name;
    /** Парк воздушных судов*/
    private Map<String, AircraftPassenger> aircraftFleet;
    /** Маршруты*/
    private Map<String, FlightRoute> routers;

    private Aircompany(){
    }

    public Aircompany(String nameCompany){
        this.name = nameCompany;
        aircraftFleet = new HashMap<String, AircraftPassenger>();
        routers = new TreeMap<String, FlightRoute>();
    }

    public String getName(){
        return this.name;
    }

    /** Добавить маршрут*/
    public void appendRoute(FlightRoute route){
        routers.put(route.getName(), route);
    }

    /** Получить маршрут*/
    public FlightRoute getRoute(String name) {
        return routers.get(name);
    }

    /** Удалить маршрут*/
    public void removeRoute(FlightRoute route){
        routers.remove(route.getName());
    }

    /** Добавить в парк новый самолет*/
    public void appendAircraft(AircraftPassenger aircraft){
        if(aircraft!=null)
            aircraftFleet.put(aircraft.getId(), aircraft);
    }

    /** Модель самолета по идентификатору*/
    public String getModelAircraft(String idAircraft){
        return aircraftFleet.get(idAircraft).getModelAircraft();
    }

    /** Вместимость самолета по идентификатору самолета*/
    public Integer getCapacityAircraft(String idAircraft){
        return aircraftFleet.get(idAircraft).getCapacityPassenger();
    }

    /** Показать парк самолетов*/
    public void showAircrafts(){
        for(Map.Entry<String, AircraftPassenger> entry: aircraftFleet.entrySet())
            System.out.format("[%s aircraft]  %s   %s(%s)\n",name,entry.getValue().getId(), entry.getValue().getModelAircraft(),
                    entry.getValue().getCapacityPassenger());
    }
}
