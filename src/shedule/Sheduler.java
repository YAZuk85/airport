package shedule;

import aircraft.AircraftPassenger;
import aircraft.InterfaceAircraft;
import flight.Flight;
//import java.util.Calendar
import java.util.*;

public class Sheduler {
    private TreeMap<String, Flight> flightsDeparture;
    private TreeMap<String, Flight> flightsArrival;
    private String name;
    private Sheduler(){
    }

    public Sheduler(String name) {
        flightsArrival = new TreeMap<>();
        flightsDeparture = new TreeMap<>();
        this.name = name;
    }

    /**
     * TODO: сделать лучше код
     * Приватный метод для сокращения кода, костыльно
     * */
    private void appendFlightToFlights(Flight flight, TreeMap<String, LinkedList<Flight>> flights) {
        if (flights.containsKey(flight.getName())) {
            flights.get(flight.getName()).add(flight);
        } else {
            LinkedList<Flight> f = new LinkedList<Flight>();
            f.add(flight);
            flights.put(flight.getName(), f);
        }
    }

    /** Добавить рейс в расписание*/
    public void registrationFlight(Flight flight) throws NullPointerException{
        if(flight.getCityArrival() == this.name) {
//            appendFlightToFlights(flight,flightsArrival);
            flightsArrival.put(flight.getName(),flight);
        }
        else if(flight.getCityDeparture() == this.name) {
//            appendFlightToFlights(flight,flightsDeparture);
            flightsDeparture.put(flight.getName(),flight);
        }
    }
    /** Получить рейс*/
    public Flight getFlight(String name){
        if(flightsArrival.containsKey(name))
            return flightsArrival.get(name);
        else if(flightsDeparture.containsKey(name))
            return flightsDeparture.get(name);
        return null;
    }

    /** Удалить рейс*/
    public void removeFlight(String name){
        if(flightsArrival.containsKey(name))
            flightsArrival.remove(name);
        else if(flightsDeparture.containsKey(name))
            flightsDeparture.remove(name);
    }


    /** Показать рейсы*/
    public void showFlights() {
        System.out.println("+++++[Arrivals]+++++");
        System.out.println(flightsArrival);
        System.out.println("+++++[Departures]+++++");
        System.out.println(flightsDeparture);
    }

    public void showFlightsLinkedList() {
        System.out.println("+++++[Arrivals]+++++");
        System.out.println(flightsArrival);
        System.out.println("+++++[Departures]+++++");
        System.out.println(flightsDeparture);
    }
    /** Показать расписание рейсов*/
    public void showSheduleFlight(Integer days) {
        System.out.println("+++++[Departures]+++++");
        for(Map.Entry<String, Flight> entry: flightsDeparture.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue().getDateTimeDeparture());
            System.out.println(entry.getValue().getPeriodicity());
            System.out.println(entry.getValue().getListDays());
            Calendar calendar = new GregorianCalendar(
                    entry.getValue().getDateTimeDeparture().getYear(),
                    entry.getValue().getDateTimeDeparture().getMonth().getValue()-1,
                    entry.getValue().getDateTimeDeparture().getDayOfMonth(),
                    entry.getValue().getDateTimeDeparture().getHour(),
                    entry.getValue().getDateTimeDeparture().getMinute(),
                    entry.getValue().getDateTimeDeparture().getSecond());
            for(Integer x: entry.getValue().getListDays()) {
                System.out.println(calendar.getTime());
                calendar.add(x, 7);
            }

//            for(int i =0; i<days; i++) {
//
//            }
        }

        System.out.println("+++++[Arrivals]+++++");
        for(Map.Entry<String, Flight> entry: flightsArrival.entrySet()){
            System.out.println(entry.getKey());
            System.out.println(entry.getValue().getDateTimeArrival());
            System.out.println(entry.getValue().getPeriodicity());
            System.out.println(entry.getValue().getListDays());
        }
    }




    /** Показать рейсы одной авиакомпании*/
    public void showFlightsOfOwner(String ownerFlight) {
    }
}
