package airport;

import shedule.Sheduler;
import aircompany.Aircompany;
import flight.Flight;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;




public class Airport {
    /** Имя аэропорта - это как правило название города+код аэропорта*/
    private String name;
    /** Авиакомпании, которые работают с этим аэропортом*/
    private Map<String, Aircompany> aircompanies;
    /** Время года наверное нужно*/
    private String season;

    private Sheduler sheduler;

    private Airport(){
    }

    public Airport(String name){
        this.name = name;
        aircompanies = new TreeMap<String, Aircompany>();
        sheduler = new Sheduler(name);
    }
    /** Добавить рейс в расписание*/
    public void registrationFlight(Flight flight) throws AirportException {
        if(flight == null) throw new AirportException(1001);
//        System.out.println(flight.getName());
        sheduler.registrationFlight(flight);
    }
    /** Удалить рейс из расписания*/
    public void removeFlight(String name) throws AirportException {
        if(name.length()==0) throw new AirportException(1002);
        sheduler.removeFlight(name);
    }
    /** Добавить авиакомпанию*/
    public void appendAircompany(Aircompany aircompany) {
        aircompanies.put(aircompany.getName(),aircompany);
    }
    /** Удалить авиакомпанию*/
    public void removeAircompany(Aircompany aircompany) {
        aircompanies.remove(aircompany.getName());
    }

    public Flight getFlight(String name) {
        return sheduler.getFlight(name);
    }

    /** Показать расписания вылетов и прилетов*/
    public void showFlights() {
        sheduler.showFlights();
    }

    public void showFlights1() {
        sheduler.showFlightsLinkedList();
    }

    public void showFlights2(){
        sheduler.showSheduleFlight(10);
    }


    /** Показать рейсы одной авиакомпании*/
    public void showFlightsOfOwner(String ownerFlight) {
        sheduler.showFlightsOfOwner(ownerFlight);
    }


}
