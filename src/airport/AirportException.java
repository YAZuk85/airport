package airport;
import java.lang.Integer;


public class AirportException extends Exception {
    public AirportException(String message) {
        super(message);
    }
    public AirportException(Integer code) {

        super("AirportException(flight error):"+code);
    }
}
