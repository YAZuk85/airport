package aircraft;
import java.lang.Integer;
public abstract class Aircraft {
    /** Вместимость перевозчика*/
    private Integer capacity;
    /** Тип перевозчика(грузовой, пассажирский)*/
    private String type;

    /** Установить вместимость*/
    protected void setCapacity(Integer capacity){
        this.capacity = capacity;
    }

    /** Получить вместимость*/
    protected Integer getCapacity(){
        return this.capacity;
    }

}



