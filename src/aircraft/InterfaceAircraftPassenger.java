package aircraft;

public interface InterfaceAircraftPassenger extends InterfaceAircraft, InterfaceFeature {
    void setCountBusiness(Integer count);
    Integer getCountBusiness();
    void setCountEconomy(Integer count);
    Integer getCountEconomy();
}
