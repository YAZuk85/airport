package aircraft;
import java.lang.Integer;

public interface InterfaceAircraft {
    /** Получить максимальную высоту полета*/
    Integer getMaxFlightAltitude();
    /** Установить максимальную высоту полета*/
    void setMaxFlightAltitude(Integer maxFlightAltitude);
    /** Получить максимальную взлетную массу*/
    Integer maxTakeoffWeight();
    /** Получить максимальную взлетную массу*/
    void setMaxTakeoffWeight(Integer maxTakeoffWeight);
}
