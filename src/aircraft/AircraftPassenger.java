package aircraft;


public class AircraftPassenger extends Aircraft implements InterfaceAircraftPassenger {
    public enum ModelAircraft { BOEING737, BOEING747, BOEING767, AIRBUS319, AIRBUS320, AIRBUS321, ATR72, ATR42, BOMBARDIER_CRJ100,
        BOMBARDIER_CRJ200 }

    /** Модель самолета(Боинг, Эйрбас)*/
    private String modelAircraft;
    /** Идентификатор самолета(уникальный бортовой номер во всем мире среди всех самолетов)*/
    private String id;
    /** Максимальная высота полеты*/
    private Integer maxFlightAltitude;
    /** Максимальный взлетный вес*/
    private Integer maxTakeoffWeight;
    /** Количество мест бизнес класса*/
    private Integer countBusiness;
    /** Количество мест эконоического класса*/
    private Integer countEconomy;


    private AircraftPassenger() {
    }

    public AircraftPassenger(String id, String model_aircraft, int capacityPassenger) {
        this.modelAircraft = model_aircraft;
        super.setCapacity(capacityPassenger);
        this.id = id;
        this.maxFlightAltitude = 0;
        this.maxTakeoffWeight = 0;
    }
    /** Получить идентификатор самолет*/
    public String getId(){
        return this.id;
    }
    /** Получить модель самолета*/
    public String getModelAircraft(){
        return this.modelAircraft;
    }
    /** Получить вместимость самолета*/
    public int getCapacityPassenger() {
        return super.getCapacity();
    }

    /** Получить максимальную высоту полета*/
    @Override
    public Integer getMaxFlightAltitude(){
        return this.maxFlightAltitude;
    }

    /** Установить максимальную высоту полета*/
    @Override
    public void setMaxFlightAltitude(Integer maxFlightAltitude){
        this.maxFlightAltitude = maxFlightAltitude;
    }

    /** Получить максимальную взлетную массу*/
    @Override
    public Integer maxTakeoffWeight(){
        return this.maxTakeoffWeight;
    }

    /** Установить максимальную взлетную массу*/
    @Override
    public void setMaxTakeoffWeight(Integer maxTakeoffWeight) {
        this.maxTakeoffWeight = maxTakeoffWeight;
    }

    /** Установить количество мест бизнес-класса*/
    @Override
    public void setCountBusiness(Integer count) {
        this.countBusiness = count;
    }
    /** Получить количество мест бизнес-класса*/
    @Override
    public Integer getCountBusiness() {
        return this.countBusiness;
    }
    /** Установить количество мест эконом-класса*/
    @Override
    public void setCountEconomy(Integer count) {
        this.countEconomy = count;
    }
    /** Получить количество мест эконом-класса*/
    @Override
    public Integer getCountEconomy() {
        return this.countEconomy;
    }
    @Override
    public void setFeature(){

    }
}
