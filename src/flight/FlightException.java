package flight;

public class FlightException extends Exception {
    public FlightException(String message) {
        super(message);
    }
    public FlightException(Integer code) {

        super("FlightException(flight error):"+code);
    }
}
