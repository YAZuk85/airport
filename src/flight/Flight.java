package flight;
import java.time.LocalDateTime;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.EnumSet;

public class Flight extends FlightRoute{
    public enum Status{OPENED, WAITING, CANCELED};

    public enum Days{DayOfWeekDAYS};

    Set<DayOfWeek> weekend = EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY,
            DayOfWeek.THURSDAY,DayOfWeek.FRIDAY,DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

    public enum Periodicity{DAILY, WEEKLY, MONTHLY};



    /** Владелец рейса - название компании*/
    private String owner;
    /** Текущее время отправления*/
    private LocalDateTime currentDateTimeDeparture;
    /** Текущее время прибытия*/
    private LocalDateTime currentDateTimeArrival;
    /** Статус рейса(открыт, задержен, отменен)*/
    private String status;
    /** Модель самолета*/
    private String modelAircraft;
    /** Бортовой номер самолета*/
    private String idAircraft;
    private String periodicity;
    private List<Integer> listDays;

    /** Конструктор по умолчанию не доступен*/
    private Flight() {
        super(null,null,null, null, null);
    }


    /** Конструктор только с параметрами*/
//    public Flight(String owner, String name,String cityDeparture, String cityArrival, LocalDateTime dateTimeDeparture, LocalDateTime dateTimeArrival,
//                  String modelAircraft, String idAircraft) {
//        super(name,cityDeparture,cityArrival, dateTimeDeparture, dateTimeArrival);
//        this.currentDateTimeDeparture = dateTimeDeparture;
//        this.currentDateTimeArrival = dateTimeArrival;
//        this.owner = owner;
//        this.status = Status.WAITING.name();
//        this.modelAircraft = modelAircraft;
//        this.idAircraft = idAircraft;
//    }

    public Flight(String owner, String name,String cityDeparture, String cityArrival,
                  LocalDateTime dateTimeDeparture, LocalDateTime dateTimeArrival,
                  String periodicity, List<Integer> listDays) {
        super(name,cityDeparture,cityArrival, dateTimeDeparture, dateTimeArrival);
        this.currentDateTimeDeparture = dateTimeDeparture;
        this.currentDateTimeArrival = dateTimeArrival;
        this.owner = owner;
        this.status = Status.WAITING.name();
        this.modelAircraft = "";
        this.idAircraft = "";
        this.periodicity = periodicity;
        this.listDays = listDays;
    }

    /** Установить статус*/
    public void setStatus(String status) {
        this.status = status;
    }
    /** Получить город назначения*/
    public String getCityArrival() {
        return super.getCityArrival();
    }
    /** Получить город отправления*/
    public String getCityDeparture() {
        return super.getCityDeparture();
    }
    /** Получить время и дату отправления*/
    public LocalDateTime getDateTimeDeparture() {
        return currentDateTimeDeparture;
    }
    /** Получить время и дату прибытия*/
    public LocalDateTime getDateTimeArrival() {
        return currentDateTimeArrival;
    }
    /** Получить название авиакомпании*/
    public String getOwner(){
        return owner;
    }
    /** Получить модель самолета*/
    public String getModelAircraft(){
        return modelAircraft;
    }
    /** Получить идентификатор самолета*/
    public String getIdAircraft(){
        return idAircraft;
    }
    /** Получить статус рейса*/
    public String getStatus(){
        return status;
    }
    /** Утснаовить статус рейса*/
    public void setStatus(Status status){
        this.status = status.name();
    }
    public String getPeriodicity(){
        return this.periodicity;
    }
    public List<Integer> getListDays(){
        return this.listDays;
    }


}
