package flight;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FlightRoute {
    /** Имя маршрута*/
    private String name;
    /** Город отправления*/
    private String cityDeparture;
    /** Город прибытия*/
    private String cityArrival;
    /** Дистанция между городами*/
//    private int distance;
    /** Текущее время отправления*/
    private LocalDateTime dateTimeDeparture;
    /** Текущее время прибытия*/
    private LocalDateTime dateTimeArrival;
    /** Периодичность ежедневно, несколько дней в неделю, еженедельно, чартер с конкретным датой в временем*/
//    private String periodicity;
    /** Дни недели если пришел такой тип периодичности*/
//    private List<String> listDays;

    private FlightRoute(){
    }
    /** Конструктор только с параметрами*/
    public FlightRoute(String name,String cityDeparture, String cityArrival,
                       LocalDateTime dateTimeDeparture, LocalDateTime dateTimeArrival){
        this.name = name;
        this.cityArrival = cityArrival;
        this.cityDeparture = cityDeparture;
        this.dateTimeArrival = dateTimeArrival;
        this.dateTimeDeparture = dateTimeDeparture;
//        this.listDays = listDays;
//        this.periodicity = periodicity;
//        this.listDays = new ArrayList<>()
//        listDays.add("asdasdada");
//        listDays.add("asdasdada");
//        listDays.add("asdasdada");
//        listDays.add("asdasdada");
//        System.out.println(this.listDays);
    }

    /** Установить имя маршрута*/
    public void setName(String name){
        this.name = name;
    }
    /** Установить город прибытия*/
    public void setCityArrival(String cityArrival){
        this.cityArrival = cityArrival;
    }
    /** Установить город отправления*/
    public void setCityDeparture(String cityDeparture){
        this.cityDeparture = cityDeparture;
    }
    /** Установить время отправления*/
    public void setDateTimeDeparture(LocalDateTime dateTimeDeparture){
        this.dateTimeDeparture = dateTimeDeparture;
    }
    /** Получить время отправления*/
    public LocalDateTime getDateTimeDeparture(){
        return this.dateTimeDeparture;
    }
    /** Установить время прибытия*/
    public void setDateTimeArrival(LocalDateTime dateTimeArrival){
        this.dateTimeArrival = dateTimeArrival;
    }
    /** Получить время прибытия*/
    public LocalDateTime getDateTimeArrival(){
        return this.dateTimeArrival;
    }

    /** Получить имя маршрута*/
    public String getName(){
        return this.name;
    }
    /** Получить город отбытия*/
    public String getCityDeparture(){
        return this.cityDeparture;
    }
    /** Получить город прибытия*/
    public String getCityArrival(){
        return this.cityArrival;
    }



}
