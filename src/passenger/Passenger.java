package passenger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class Passenger {
    /** Полное имя*/
    public String fullName;
    /** Паспортные данные*/
    private String passport;
    /** Дата рождения*/
    private LocalDate birthday;
    /** Номер рейса*/
    private String flight;

    /** Конструктор по умолчанию не доступен*/
    private Passenger() {

    }

    public Passenger(String fullName, String passport, LocalDate birthday, String flight) {
        this.fullName = fullName;
        this.passport = passport;
        this.birthday = birthday;
        this.flight = flight;
    }
    /** Получить номер рейса*/
    public String getFlight() {
        return this.flight;
    }
    /** Получить полное имя пассажира*/
    public String getFullName() {
        return this.fullName;
    }
    /** Получить паспортные данные*/
    public String getPassport() {
        return this.passport;
    }
    /** Получить дату рождения*/
    public LocalDate getBirthday() {
        return this.birthday;
    }
    /** Установить имя*/
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    /** Установить паспортные данные*/
    public void setPassport(String passport) {
        this.passport = passport;
    }
    /** Установить дату рождения*/
    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
    /** Установить номер рейса*/
    public void setFlight(String flight) {
        this.flight = flight;
    }

    public void showInfo() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        System.out.format("Fullname:%s Passport:%s Birthday:%s Flight:%s",
                this.fullName, this.passport, formatter.format(this.birthday), this.flight);
    }

}
