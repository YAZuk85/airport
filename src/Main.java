import java.time.LocalDateTime;

import airport.AirportException;
import flight.Flight;
import aircraft.AircraftPassenger;
import aircompany.Aircompany;
import airport.Airport;
import flight.FlightRoute;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Main {
    public enum Days{DayOfWeekDAYS};

    public static void main(String[] args) {

        System.out.println("**********Airport***********");

        List<Integer> listDays1 = new ArrayList<>();
        listDays1.add(Calendar.SUNDAY);
        listDays1.add(Calendar.TUESDAY);
        listDays1.add(Calendar.WEDNESDAY);

        List<Integer> listDays2 = new ArrayList<>();
        listDays2.add(Calendar.MONDAY);
        listDays2.add(Calendar.THURSDAY);




        /** Создание авиакомпаний*/
        Aircompany aircompanyAeroflot = new Aircompany("Aeroflot");

        /** Заполнение авиакомпаний самолетами*/
        aircompanyAeroflot.appendAircraft(new AircraftPassenger("JMZGGNBVBVCFD1",
                AircraftPassenger.ModelAircraft.BOEING737.name(),205));
        aircompanyAeroflot.appendAircraft(new AircraftPassenger("JMZGGKJHKJHKJ2",
                AircraftPassenger.ModelAircraft.BOEING747.name(),210));
        aircompanyAeroflot.appendAircraft(new AircraftPassenger("JMZGGIUYIUGFH3",
                AircraftPassenger.ModelAircraft.BOEING767.name(),190));
        aircompanyAeroflot.appendAircraft(new AircraftPassenger("JMZGGIUIUIUIU4",
                AircraftPassenger.ModelAircraft.AIRBUS319.name(),185));
        aircompanyAeroflot.appendAircraft(new AircraftPassenger("JMZGGUYTUYTUY5",
                AircraftPassenger.ModelAircraft.AIRBUS321.name(),215));

        aircompanyAeroflot.appendRoute(new FlightRoute("SU178","Moscow(SVO)","Penza(PEZ)",
                LocalDateTime.of(2018,11,21,12,0),
                LocalDateTime.of(2018,11,21,14,0))
        );
        aircompanyAeroflot.appendRoute(new FlightRoute("SU278","Moscow(SVO)","Saratov(RTW)",
                LocalDateTime.of(2018,11,22,16,30),
                LocalDateTime.of(2018,11,22,19,15))
        );
        aircompanyAeroflot.appendRoute(new FlightRoute("SU154","Moscow(SVO)","Krasnoyarsk(KRS)",
                LocalDateTime.of(2018,11,25,9,0),
                LocalDateTime.of(2018,11,25,13,45))
        );
        aircompanyAeroflot.appendRoute(new FlightRoute("SU131","Moscow(SVO)","Murmansk(MNS)",
                LocalDateTime.of(2018,11,20,11,0),
                LocalDateTime.of(2018,11,20,13,0))
        );

        aircompanyAeroflot.appendRoute(new FlightRoute("SU888","Murmansk(MNS)","Moscow(SVO)",
                LocalDateTime.of(2018,12,20,3,20),
                LocalDateTime.of(2018,12,20,6,45))
        );
        aircompanyAeroflot.appendRoute(new FlightRoute("SU777","Saratov(RTW)","Moscow(SVO)",
                LocalDateTime.of(2018,11,20,11,0),
                LocalDateTime.of(2018,11,20,13,0))
        );
        aircompanyAeroflot.appendRoute(new FlightRoute("SU257","Penza(PEZ)","Moscow(SVO)",
                LocalDateTime.of(2018,12,22,11,0),
                LocalDateTime.of(2018,12,22,13,15))
        );


        /** Покажем парк авиакомпаний*/
//        aircompanyAeroflot.showAircrafts();

        Airport airport = new Airport("Moscow(SVO)");

        try {
            airport.registrationFlight(new Flight(aircompanyAeroflot.getName(),
                    aircompanyAeroflot.getRoute("SU278").getName(),
                    aircompanyAeroflot.getRoute("SU278").getCityDeparture(),
                    aircompanyAeroflot.getRoute("SU278").getCityArrival(),
                    aircompanyAeroflot.getRoute("SU278").getDateTimeDeparture(),
                    aircompanyAeroflot.getRoute("SU278").getDateTimeArrival(), "weekly", listDays1));


            airport.registrationFlight(new Flight(aircompanyAeroflot.getName(),
                    aircompanyAeroflot.getRoute("SU154").getName(),
                    aircompanyAeroflot.getRoute("SU154").getCityDeparture(),
                    aircompanyAeroflot.getRoute("SU154").getCityArrival(),
                    aircompanyAeroflot.getRoute("SU154").getDateTimeDeparture(),
                    aircompanyAeroflot.getRoute("SU154").getDateTimeArrival(), "weekly", listDays2));


            airport.registrationFlight(new Flight(aircompanyAeroflot.getName(),
                    aircompanyAeroflot.getRoute("SU888").getName(),
                    aircompanyAeroflot.getRoute("SU888").getCityDeparture(),
                    aircompanyAeroflot.getRoute("SU888").getCityArrival(),
                    aircompanyAeroflot.getRoute("SU888").getDateTimeDeparture(),
                    aircompanyAeroflot.getRoute("SU888").getDateTimeArrival(),"charter", null));

            airport.registrationFlight(new Flight(aircompanyAeroflot.getName(),
                    aircompanyAeroflot.getRoute("SU257").getName(),
                    aircompanyAeroflot.getRoute("SU257").getCityDeparture(),
                    aircompanyAeroflot.getRoute("SU257").getCityArrival(),
                    aircompanyAeroflot.getRoute("SU257").getDateTimeDeparture(),
                    aircompanyAeroflot.getRoute("SU257").getDateTimeArrival(),"daily", null));



//            airport.registrationFlight(null);
        }
        catch (AirportException exc) {
            System.out.println(exc.getMessage());
        }
//        catch (NullPointerException exc) {
//            System.out.println(exc.getMessage());
//        }

        finally {
            System.out.println("Finally append flight");
        }

//        try {
//            airport.removeFlight("");
//        }
//
//        catch (AirportException exc) {
//            System.out.println(exc.getMessage());
//        }
//        finally {
//            System.out.println("Finally remove flight");
//        }

        airport.showFlights2();

    }
}

